# Steam SMTP
Custom built SMTP application to handle Steam's e-mails. Severly limited at the moment and the code is a mess.

## Todo
- Accessible only through local interface (to prevent foreign machines accessing steam guard codes etc.)
- Refactor

## Purpose
- Provide Steam Guard codes
- Automatically activate account when e-mail arrives
- Provide codes for password reset
