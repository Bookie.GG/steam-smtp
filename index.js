var fs = require('fs');
var simplesmtp = require('simplesmtp');
var MailParser = require('mailparser').MailParser;
var express = require('express');
var request = require('request');

var app = express();

var mail_keys = {};
var steam_guard_keys = {};
var pw_change_keys = {};
var pw_reset_keys = {};

console.log("Starting SMTP");

if(process.env.USER !== "root") {
    console.warn("WARNING: SMTP is not running under root user. It will most likely not bind to NIC.");
}

/**
 * Automatically remove the key in 60 minutes
 * because at that point it's better to generate new mail
 *
 * @param username
 * @param key
 */
function add_pw_reset_key(username, key) {
    username = username.toLowerCase();
    pw_reset_keys[username] = key;
    setTimeout(function(un) {
        delete pw_reset_keys[un];
    }, 60 * 60 * 1000, username);
}

/**
 * Automatically remove the key in 60 minutes
 * because at that point it's better to generate new mail
 *
 * @param username
 * @param key
 */
function add_steam_guard_key(username, key) {
    username = username.toLowerCase();
    steam_guard_keys[username] = key;
    setTimeout(function(un) {
        delete steam_guard_keys[un];
    }, 60 * 60 * 1000, username);
}

/**
 * Automatically remove the key in 60 minutes
 * because at that point it's better to generate new mail
 *
 * @param username
 * @param key
 */
function add_mail_change_key(username, key) {
    username = username.toLowerCase();
    mail_keys[username] = key;
    setTimeout(function(un) {
        delete mail_keys[un];
    }, 60 * 60 * 1000, username);
}

/**
 * Automatically remove the key in 60 minutes
 * because at that point it's better to generate new mail
 *
 * @param username
 * @param key
 */
function add_pw_change_key(username, key) {
    username = username.toLowerCase();
    pw_change_keys[username] = key;
    setTimeout(function(un) {
        delete pw_change_keys[un];
    }, 60 * 60 * 1000, username);
}

app.get('/mail_change/:botname', function(req, res) {
    var botname = req.params.botname.toLowerCase();
    if(botname in mail_keys) {
        res.send(mail_keys[botname]);
    } else {
        res.statusCode = 404;
        res.send('404');
    }
});

app.get('/pw_reset/:botname', function(req, res) {
    var botname = req.params.botname.toLowerCase();
    if(botname in pw_reset_keys) {
        res.send(pw_reset_keys[botname]);
    } else {
        res.statusCode = 404;
        res.send('404');
    }
});

app.get('/pw_change/:botname', function(req, res) {
    var botname = req.params.botname.toLowerCase();
    if(botname in pw_change_keys) {
        res.send(pw_change_keys[botname]);
    } else {
        res.statusCode = 404;
        res.send('404');
    }
});

app.get('/steam_guard/:botname', function(req, res) {
    var botname = req.params.botname.toLowerCase();
    if(botname in steam_guard_keys) {
        res.send(steam_guard_keys[botname]);
    } else {
        res.statusCode = 404;
        res.send('404');
    }
});

app.listen(8025);

function get_botname(address) {
    var botname_match = address.match(/([^@]+)/);
    if(botname_match == null) {
        return false;
    } else {
        return botname_match[1];
    }
}

function get_steam_guard_key(text) {
    var prepend = "Here's the Steam Guard code you'll need to complete the process: *";
    var key_match = text.match(new RegExp(prepend + '([A-Z0-9]+)\\n'));
    if(key_match == null) {
        return false;
    } else {
        return key_match[1];
    }
}

function get_steam_mail_change_key(text) {
    var is_mailchange = text.match(/contact email address associated with your Steam account?/);

    if(!is_mailchange) {
        console.log("no match");
        return false;
    }

    var mail_change_key_match = text.match(/left open in Steam: ([A-Z0-9]+)/);
    if(mail_change_key_match == null) {
        return false;
    }

    return mail_change_key_match[1];
}

function get_password_change_key(text) {
    var is_pwchange = text.match(/Have you made a request to update the password/);

    if(!is_pwchange) return;

    var key = text.match(/Steam:\n\n([0-9A-Z]+)\n\n/);

    return key[1];
}

function is_sender_steam(senders) {
    if(senders.length != 1) return false;

    var steam_mails = ["noreply@steampowered.com", "support@steampowered.com"];

    var sender = senders[0];
    return (sender.address == steam_mails[0] ||  sender.address == steam_mails[1]);
}

function get_steam_verify_mail_url(html) {
    var match = html.match(/http:\/\/steamcommunity.com\/actions\/validateemail\?stoken=[0-9_\-]+/);
    if(!match) return false;
    return match[0];
}

function get_password_reset_key(html) {
    var match = html.match(/reset your Steam password/);
    if(!match) return false;

    var key = html.match(/\n\n *([A-Z0-9]+)\n\n/);

    return key[1]
}

function verify_email(url, on_end) {
    request(url, function(err, response, body) {
        if(err) throw err;
        if(response.statusCode == 200) {
            on_end(true);
        } else {
            on_end(false);
        }
    })
}

simplesmtp.createSimpleServer({SMTPBanner:"Bookie.GG SMTP Server"}, function(req){
    var mailparser = new MailParser();

    mailparser.on('end', function(mail) {
        if(is_sender_steam(mail.from)) {
            console.log("Steam mail registered");
            var botname = get_botname(mail.to[0].address);

            if(!botname) {
                console.log("Unknown botname: " + mail.to[0].address);
            } else {
                var key, url;
                if(key = get_steam_guard_key(mail.text)) {
                    console.log("Steam guard key received!");
                    console.log("Botname: " + botname);
                    console.log("Steam key: " + key);
                    add_steam_guard_key(botname, key);
                } else if(key = get_steam_mail_change_key(mail.text)) {
                    console.log("Steam mail change received!");
                    console.log("Botname: Bookie" + botname);
                    console.log("Mail change key: " + key);
                    add_mail_change_key(botname, key);
                } else if(key = get_password_change_key(mail.text)) {
                    console.log("PW Change key: " + key);
                    add_pw_change_key(botname, key);
                } else if(key = get_password_reset_key(mail.text)) {
                    console.log("PW Reset key: " + key);
                    add_pw_reset_key(botname, key);
                } else if(url = get_steam_verify_mail_url(mail.text)) {
                    verify_email(url, function(success) {
                        if(success) {
                            console.log("Email " + mail.to[0].address + " has been successfully verified.");
                        } else {
                            console.error("Email " + mail.to[0].address + " was not verified.");
                        }
                    });
                } else {
                    fs.readdir('./unhandled_mails', function(err, files) {
                        var count = files.length;
                        fs.writeFile('./unhandled_mails/' + count + ".mail", JSON.stringify(mail), function(err) {
                            if(err) throw err;
                            console.log("Unhandled e-mail has been saved");
                        });
                    });
                }
            }
        } else console.log("Non-steam email received");
    });

    req.pipe(mailparser);
    req.accept();
}).listen(25);
